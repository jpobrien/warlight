/**
 * Warlight AI Game Bot
 *
 * Last update: April 02, 2014
 *
 * @author Jim van Eeden
 * @version 1.0
 * @License MIT License (http://opensource.org/Licenses/MIT)
 */

package main;

import java.util.LinkedList;
import java.util.Queue;

import bot.BotState;


public class Region {
	
	private int id;
	private LinkedList<Region> neighbors;
	private SuperRegion superRegion;
	private int armies;
	private String playerName;
	private int distance;
	
	public Region(int id, SuperRegion superRegion)
	{
		this.id = id;
		this.superRegion = superRegion;
		this.neighbors = new LinkedList<Region>();
		this.playerName = "unknown";
		this.armies = 0;
		
		superRegion.addSubRegion(this);
	}
	
	public Region(int id, SuperRegion superRegion, String playerName, int armies)
	{
		this.id = id;
		this.superRegion = superRegion;
		this.neighbors = new LinkedList<Region>();
		this.playerName = playerName;
		this.armies = armies;
		
		superRegion.addSubRegion(this);
	}
	
	public void addNeighbor(Region neighbor)
	{
		if(!neighbors.contains(neighbor))
		{
			neighbors.add(neighbor);
			neighbor.addNeighbor(this);
		}
	}
	
	/**
	 * @param region a Region object
	 * @return True if this Region is a neighbor of given Region, false otherwise
	 */
	public boolean isNeighbor(Region region)
	{
		if(neighbors.contains(region))
			return true;
		return false;
	}

	/**
	 * @param playerName A string with a player's name
	 * @return True if this region is owned by given playerName, false otherwise
	 */
	public boolean ownedByPlayer(String playerName)
	{
		if(playerName.equals(this.playerName))
			return true;
		return false;
	}
	
	/**
	 * @param armies Sets the number of armies that are on this Region
	 */
	public void setArmies(int armies) {
		this.armies = armies;
	}
	
	/**
	 * @param playerName Sets the Name of the player that this Region belongs to
	 */
	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
	
	/**
	 * @return The id of this Region
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * @return A list of this Region's neighboring Regions
	 */
	public LinkedList<Region> getNeighbors() {
		return neighbors;
	}
	
	/**
	 * @return The SuperRegion this Region is part of
	 */
	public SuperRegion getSuperRegion() {
		return superRegion;
	}
	
	/**
	 * @return The number of armies on this region
	 */
	public int getArmies() {
		return armies;
	}
	
	/**
	 * @return A string with the name of the player that owns this region
	 */
	public String getPlayerName() {
			return playerName;
	}
	
	public void setDistanceToBorder(int distance) {
		this.distance = distance;		
	}

	public int getDistanceToBorder() {
		return distance;
	}
	
	public int getPlacePrefrence(String name){
		int prefrence = 0;
		for(int i = 0; i < neighbors.size(); i++){
			if(neighbors.get(i).ownedByPlayer(name)){
				prefrence++;
			}
		}
		return prefrence;
	}
	
	public int getAdjustedRegionValue(BotState state) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public Region transferPicker(LinkedList<Region> borders, Region start, Map map){
		boolean[] visited = new boolean[map.regions.size()];
		int length = 1000;
		Region transfer = start.getNeighbors().get(0);
		for(int i = 0; i < borders.size(); i++){
			Queue<Region> path = new LinkedList<Region>();
		}
		return transfer;
	}

	public LinkedList<Region> getOwnedNeighbors(BotState state) {
		LinkedList<Region> neighbors = new LinkedList<Region>();
		for(int i = 0; i < state.getVisibleMap().getUserRegions(state.getMyPlayerName()).size(); i++){
			for(int j = 0; j < state.getVisibleMap().getUserRegions(state.getMyPlayerName()).get(i).getNeighbors().size(); j++){
				if(state.getVisibleMap().getUserRegions(state.getMyPlayerName()).get(i).getNeighbors().get(j).ownedByPlayer(state.getMyPlayerName()))
					neighbors.add(state.getVisibleMap().getUserRegions(state.getMyPlayerName()).get(i).getNeighbors().get(j));
			}
		}
		
		return neighbors;
	}

}
