/**
 * Warlight AI Game Bot
 *
 * Last update: April 02, 2014
 *
 * @author Jim van Eeden
 * @version 1.0
 * @License MIT License (http://opensource.org/Licenses/MIT)
 */

package bot;

/**
 * This is a simple bot that does random (but correct) moves.
 * This class implements the Bot interface and overrides its Move methods.
 *  You can implements these methods yourself very easily now,
 * since you can retrieve all information about the match from variable â€œstateâ€�.
 * When the bot decided on the move to make, it returns an ArrayList of Moves. 
 * The bot is started by creating a Parser to which you add
 * a new instance of your bot, and then the parser is started.
 */

import java.util.ArrayList;
import java.util.LinkedList;

import main.Region;
import main.SuperRegion;
import move.AttackTransferMove;
import move.PlaceArmiesMove;

public class BotStarter implements Bot 
{
	@Override
	/**
	 * A method used at the start of the game to decide which player start with what Regions. 6 Regions are required to be returned.
	 * This example randomly picks 6 regions from the pickable starting Regions given by the engine.
	 * @return : a list of m (m=6) Regions starting with the most preferred Region and ending with the least preferred Region to start with 
	 */
	public ArrayList<Region> getPreferredStartingRegions(BotState state, Long timeOut)
	{
		int m = 6;
		ArrayList<Region> preferredStartingRegions = new ArrayList<Region>();
		for(int i=0; i<m; i++)
		{
			double rand = Math.random();
			int r = (int) (rand*state.getPickableStartingRegions().size());
			int regionId = state.getPickableStartingRegions().get(r).getId();
			Region region = state.getFullMap().getRegion(regionId);

			if(!preferredStartingRegions.contains(region))
				preferredStartingRegions.add(region);
			else
				i--;
		}
		
		return preferredStartingRegions;
	}

	@Override
	/**
	 * This method is called for at first part of each round. This example puts two armies on random regions
	 * until he has no more armies left to place.
	 * @return The list of PlaceArmiesMoves for one round
	 */
	public ArrayList<PlaceArmiesMove> getPlaceArmiesMoves(BotState state, Long timeOut) 
	{
		
		ArrayList<PlaceArmiesMove> placeArmiesMoves = new ArrayList<PlaceArmiesMove>();
		String myName = state.getMyPlayerName();
		int armiesLeft = state.getStartingArmies();
		int armies = armiesLeft;
		LinkedList<Region> userRegions = state.getVisibleMap().getUserRegions(myName);
		
		for(int i = 0; i < userRegions.size(); i++){
			LinkedList<Region> neighbors = userRegions.get(i).getNeighbors();
			for(int j = 0; j < neighbors.size(); j++){
				if(!neighbors.get(j).ownedByPlayer(myName)){
					placeArmiesMoves.add(new PlaceArmiesMove(myName, userRegions.get(i), armies));
					return placeArmiesMoves;
				}
			}
		}	
		return placeArmiesMoves;
	}

	@Override
	/**
	 * This method is called for at the second part of each round. This example attacks if a region has
	 * more than 6 armies on it, and transfers if it has less than 6 and a neighboring owned region.
	 * @return The list of PlaceArmiesMoves for one round
	 */
	public ArrayList<AttackTransferMove> getAttackTransferMoves(BotState state, Long timeOut) 
	{
		ArrayList<AttackTransferMove> attackTransferMoves = new ArrayList<AttackTransferMove>();
		String myName = state.getMyPlayerName();
		LinkedList<Region> userRegions = state.getVisibleMap().getUserRegions(myName);
		
		for(int i = 0; i < userRegions.size(); i++){
			Region curr = userRegions.get(i);
			for(int j = 0; j < curr.getNeighbors().size(); j++){
				Region next = curr.getNeighbors().get(j);
				if(!next.ownedByPlayer(myName))
					if(next.getArmies() < curr.getArmies()*.6){
						attackTransferMoves.add(new AttackTransferMove(myName, curr, next, curr.getArmies()-1));
						break;
					}
			}
			attackTransferMoves.add(new AttackTransferMove(myName, curr, DistanceToBorderCalculator.getClosestNeighborToBorder(state, curr), (curr.getArmies())/2));
		}
		return attackTransferMoves;
	}
	
	public static void main(String[] args)
	{
		BotParser parser = new BotParser(new BotStarter());
		parser.run();
	}

}
